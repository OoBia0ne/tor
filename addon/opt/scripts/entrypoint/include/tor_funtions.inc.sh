#!/bin/bash

#########################################
## PREPARING TOR
#########################################
chown ${TOR_USR}:${TOR_GID} ${TOR_CERT_DIR} -R
chmod 700 ${TOR_CERT_DIR}
echo "OK :: PERMISSIONS [ ${TOR_CERT_DIR} ] SET"

#########################################
## DON NOT RUN AS DAEMON
#########################################
sed -i s/RUN_DAEMON=\"yes\"/RUN_DAEMON=\"no\"/g /etc/default/tor
echo "DON NOT RUN AS DAEMON... [ /etc/default/tor ] MODIFIED"


#################################
## ONLY IF PROXY
#################################
if [ "${TOR_SOCKS_ACCEPT_HOSTS}" ]; then
	echo "HTTPTunnelPort 0.0.0.0:8123" > /etc/tor/torrc
	echo "SocksPort 0.0.0.0:9050" >> /etc/tor/torrc

	for ACCEPT_HOST in ${TOR_SOCKS_ACCEPT_HOSTS}
	do
		echo "SocksPolicy accept ${ACCEPT_HOST}" >> /etc/tor/torrc
	done
	echo "OK :: TOR PROXY CONFIG [ /etc/tor/torrc ] CREATED"

	################################################################
	## FORCE USING EXIT NODES
	## 2-letter ISO3166 codes 
	## https://www.nationsonline.org/oneworld/country_code_list.htm
	## https://hackertarget.com/tor-exit-node-visualization/
	################################################################
	if [ "${EXCLUDE_EXIT_NODES}" ]; then
		for EXCLUDE_EXIT_NODE in ${EXCLUDE_EXIT_NODES}; do
		        NODES+="{${EXCLUDE_EXIT_NODE}},"
		done
		## TRANSFORMING CC UPPTER RO lower CASE 
		NODES=$( echo "${NODES}" | sed s/,$//g | tr -s '[:upper:]' '[:lower:]' )
		echo "ExcludeNodes ${NODES}" >> /etc/tor/torrc
		## echo "StrictNodes 1"  >> /etc/tor/torrc
	fi
fi

#################################
## CREATE A HIDDEN SERVICE
#################################
if [ "${TOR_HOST}" ]; then
	echo "HiddenServiceDir ${TOR_CERT_DIR}" > /etc/tor/torrc

	for TOR_PORT in ${TOR_PORTS}
	do
		echo "HiddenServicePort ${TOR_PORT} ${TOR_HOST}:${TOR_PORT}" >> /etc/tor/torrc
	done
	echo "OK :: HIDDEN SERVICE [ ${TOR_CERT_DIR} ] CREATED"
fi

