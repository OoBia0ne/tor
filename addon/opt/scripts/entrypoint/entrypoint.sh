#!/bin/bash
####################################################################
##
## POSTSCRIPT FOR EXECUTING TASKS TO BE DONE DURING 1ST START
## SCRIPT USED FOR
## KUBERNETES / DOCKER
##
## MAINTAINER:		
##			
## VERSION:		0.0.1
## LICENCE:		GPLv3
##
####################################################################
## set -xn

## BASE DIR
MY_DIR="/opt/scripts/entrypoint"

cat <<EOF

###################################################################
 SYSTEM:		${HOSTNAME}
###################################################################

EOF

#############################################
## GET ALL FILES TO BE INCLUDED
#############################################
INCLUDE_FILES=$( ls ${MY_DIR}/include/ | grep .inc.sh )
for FILE in ${INCLUDE_FILES}
do
        . ${MY_DIR}/include/${FILE}
done

echo "STARTING TOR..."
sudo -u ${TOR_USR:-debian-tor} /usr/bin/tor 
