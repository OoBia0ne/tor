###################################
## SETTING START POINT 
###################################
FROM debian:bookworm-slim
ENV DEBIAN_FRONTEND=noninteractive

###################################
## INSTALLING DEPENDENCIES 
###################################
RUN /usr/bin/apt-get update
RUN /usr/bin/apt-get install -y --no-install-recommends wget gnupg gpg ca-certificates apt-utils sudo apt-transport-https 

RUN echo 'deb     [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org bookworm main'  > /etc/apt/sources.list.d/tor.list
RUN wget --no-check-certificate -qO- https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --dearmor > /usr/share/keyrings/tor-archive-keyring.gpg 

###################################
## GETTING NEEDET DEPENDENCIES
###################################
RUN /usr/bin/apt-get update 

RUN /usr/bin/apt-get install -y --no-install-recommends tor tor-geoipdb
RUN /usr/bin/apt-get clean && /usr/bin/apt-get autoremove
RUN /usr/bin/apt-cache policy tor
###################################
## INSTALLING TEMPLATES 
## AND OWNCLOUD SERVICES
###################################
RUN mkdir /addon
COPY addon/opt/ /opt

###################################
## EXECUTING PRESCRIPT
## - SETTING PERMISSIONS .... 
###################################
RUN chmod +x /opt/scripts/entrypoint/entrypoint.sh

###################################
## SETTING UP ENTRYPOINT
###################################
CMD /opt/scripts/entrypoint/entrypoint.sh 
EXPOSE          8123/TCP
EXPOSE          9050/TCP
EXPOSE          9051/TCP

