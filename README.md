# TOR - Proxy

This proxy is designed to work with [TDL](https://gitlab.com/OoBia0ne/tdl)

It ist not 100% secure because it is mainly a HttpTunnel Proxy listening on Port http://8123:TCP
Thats because of it is designed to work with TDL. TDL uses the [gotenberg](https://hub.docker.com/r/thecodingmachine/gotenberg) container which is actually not able to use socks5 but only http as proxy tunnel protocol.

But it also can be used as a socks5 proxy for other Applications.

Tested with nextcloud.
Add the following line to your config.php

```php
   'proxy' => 'socks5://127.0.0.1:9050',
```

# ENV

```bash
## ACCEPTING HOST FROM FOLLOWING NETWORKS
TOR_SOCKS_ACCEPT_HOSTS="192.168.0.0/16 10.0.0.0/8 172.16.0.0/12"

## PROXYMODE ONLY
## https://www.nationsonline.org/oneworld/country_code_list.htm
EXCLUDE_EXIT_NODES="AG US CH"
```

Exitnodes mapping to CountyCodes given in _EXCLUDE_EXIT_NODES_ will not be used as exit nodes.

# COUNTRY CODES

## https://www.nationsonline.org/oneworld/country_code_list.htm

A list of available country codes readbale by tor - proxy
The code will be translated from upper to lowercase in entrypoint.

# Setup tor-proxy container with docker-compose

```bash
git clone https://gitlab.com/OoBia0ne/tor
cd tor/
docker-compose up -d
```

# Requirements

See [TDL](https://gitlab.com/OoBia0ne/tdl) - Project for how to use with tdl as a pod
